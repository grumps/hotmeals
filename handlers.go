// handlers.go

package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func HotMealHandler(t *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// render form.
		d := make(map[string]string)
		d["page_title"] = "Hot Meals Program"
		if err := t.Execute(w, d); err != nil {
			log.Fatal(err)
		}
	}
}

func CreateHotMealHandler(t *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		fmt.Println(r.Form)
		// look up user

		// create meal
		// validate
		// save
		// render ok
	}
}
