// app.go

package main

import (
	"database/sql"
	"fmt"
	"github.com/gorrila/mux"
	"html/template"
	"log"
	"net/http"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

var createHotmealt = template.Must(template.ParseFiles("templates/hotmeal/create.html"))

func (a *App) Initialize(app_db_name string) {
	dbPath :=
		fmt.Sprintf("%s", app_db_name)
	var err error
	a.DB, err = sql.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatal(err)
	}
	a.setRoutes()
}

func (a *App) loadTemplates() {
	// TODO
}
func (a *App) setRoutes() {
	// this needs to return....
	a.Router = mux.NewRouter()
	a.Router.HandleFunc("/hotmeal/create/", middleWrap(HotMealHandler(createHotmealt))).Methods("GET")
	a.Router.HandleFunc("/hotmeal/create/", middleWrap(CreateHotMealHandler(createHotmealt))).Methods("POST")
}

func (a *App) Run(addr string) {
	fmt.Printf("starting hotmeals webserver")
	log.Fatal(http.ListenAndServe(":8080", a.Router))
}

func middleWrap(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get url load template
		defer log.Printf(r.URL.Path)
		f(w, r)
	}
}
