// model.go

package main

import (
	"database/sql"
	"errors"
	_ "github.com/mattn/go-sqlite3"
	"time"
)

type participant struct {
	ID         int    "id"
	FirstName  string "first_name"
	LastName   string "last_name"
	ExternalId string "external_id"
}

type hotmeal struct {
	ID            int       "id"
	MealTime      int       "meal_time_id"
	ParticipantId int       "participant_id"
	CreatedAt     time.Time "created_at"
}

type mealtime struct {
	ID   int    "id"
	Name string "name"
}

func (p *participant) getParticipant(db *sql.DB) error {
	return errors.New("Not implemented")
}

func (p *participant) updateParticipant(db *sql.DB) error {
	return errors.New("Not implemented")
}

func (p *participant) deleteParticipant(db *sql.DB) error {
	return errors.New("Not implemented")
}

func (p *participant) createProduct(db *sql.DB) error {
	return errors.New("Not implemented")
}

func getParticipants(db *sql.DB, start, count int) ([]participant, error) {
	return nil, errors.New("Not implemented")
}

const createHotmealQuery = `INSERT INTO hotmeal (meal_time_id,
												 participant_id,
												 created_at)
							VALUES ($1, $2, $3);`

func (h *hotmeal) createHotmeal(db *sql.DB) error {
	_, err := db.Exec(createHotmealQuery, h.MealTime, h.ParticipantId, h.CreatedAt)
	if err != nil {
		return err
	}
	return nil
}
