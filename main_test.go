// main_test

package main_test

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/grumps/hotmeals"
)

var a main.App

func TestMain(m *testing.M) {
	a = main.App{}
	a.Initialize(
		os.Getenv("TEST_APP_DB_NAME"))
	ensureTableExists()

	code := m.Run()

	clearTable()

	os.Exit(code)
}

func TestEmptyTable(t *testing.T) {
	clearTable()

	req, _ := http.NewRequest("GET", "/participants", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body != "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	}
}

func TestCreateExistentParticipantService(t *testing.T) {
	clearTable()
	req, _ := http.NewRequest("POST", "/participant/11/", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusCreated, response.Code)

	//var m map[string]string
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr

}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func ensureTableExists() {
	if _, err := a.DB.Exec(tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	a.DB.Exec("DELETE FROM participants")
}

const tableCreationQuery = `CREATE TABLE IF NOT EXISTS participants
(
	id INTEGER PRIMARY KEY,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	external_id TEXT
)`
